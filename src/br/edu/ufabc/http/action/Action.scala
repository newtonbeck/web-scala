package br.edu.ufabc.http.action

import scala.util.Failure
import scala.util.Success
import scala.util.Try
import br.edu.ufabc.http.request.Request
import br.edu.ufabc.http.response.Response
import br.edu.ufabc.http.response.OkResponse

case class Action(function: (Request) => (String)) {

  def execute(request: Request): Try[String] = {
    try {
      Success(function(request))
    } catch {
      case e: Throwable => Failure(e)
    }
  }

}