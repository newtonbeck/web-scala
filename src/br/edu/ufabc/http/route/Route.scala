package br.edu.ufabc.http.route

import br.edu.ufabc.http.request.method.HttpMethod

case class Route(val method: HttpMethod, val path: String)