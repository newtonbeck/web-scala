package br.edu.ufabc.http.route

import br.edu.ufabc.http.request.method.HttpMethod
import br.edu.ufabc.http.request.method.HttpMethodFactory

object RouteFactory {

  def get(line: String): Option[Route] = {
    val words = line.split(" ")

    if (words.size != 3)
      None
    else {
      HttpMethodFactory.get(words(0)) match {
        case Some(m) => Some(Route(m, words(1)))
        case None    => None
      }
    }
  }

}