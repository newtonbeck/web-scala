package br.edu.ufabc.http.request

import br.edu.ufabc.http.route.Route
import scala.collection.immutable.Map

case class Request(val route: Route, private val headers: Map[String, String]) {

  def header(name: String): Option[String] = {
    headers get name
  }

}