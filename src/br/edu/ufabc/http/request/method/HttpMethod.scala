package br.edu.ufabc.http.request.method

/**
 * HTTP methods superclass.
 */
sealed abstract class HttpMethod

/**
 * GET HTTP method.
 */
case object Get extends HttpMethod
