package br.edu.ufabc.http.request.method

object HttpMethodFactory {

  def get(method: String): Option[HttpMethod] = method match {
    case "GET"  => Some(Get)
    case _      => None
  }

}