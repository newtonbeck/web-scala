package br.edu.ufabc.http.request

import br.edu.ufabc.http.route.RouteFactory

object RequestFactory {

  def get(httpMessage: String): Option[Request] = {
    val lines = httpMessage.split("\r\n").toList

    if (lines.isEmpty)
      None
    else {
      RouteFactory.get(lines(0)) match {
        case Some(route) => {
          val emptyLineIndex = lines.indexOf("")
          val headers = (for (line <- lines.slice(1, emptyLineIndex))
            yield (line.split(":")(0).trim(), line.split(":")(1).trim())).toMap
          Some(Request(route, headers))
        }
        case None => None
      }
    }

  }

}