package br.edu.ufabc.http.response

import br.edu.ufabc.http.response.status.InternalServerError

case class InternalServerErrorResponse(private val exception: Throwable) extends Response {

  def status = InternalServerError

  def body = "Internal Server Error caused by: %s".format(exception.getMessage)

}