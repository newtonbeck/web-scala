package br.edu.ufabc.http.response

import br.edu.ufabc.http.response.status.HttpStatus

trait Response {

  def status: HttpStatus

  def body: String

  def toHTTP: String = {
    "HTTP/1.1 %d %s\r\n\r\n%s".format(status.code, status.text, body)
  }

}