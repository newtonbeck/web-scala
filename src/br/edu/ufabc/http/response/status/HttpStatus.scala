package br.edu.ufabc.http.response.status

sealed abstract class HttpStatus(val code: Int, val text: String)

case object Ok extends HttpStatus(200, "OK")

case object BadRequest extends HttpStatus(400, "Bad Request")

case object NotFound extends HttpStatus(404, "Not Found")

case object InternalServerError extends HttpStatus(500, "Internal Server Error")