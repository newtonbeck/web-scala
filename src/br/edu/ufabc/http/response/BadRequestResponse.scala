package br.edu.ufabc.http.response

import br.edu.ufabc.http.response.status.BadRequest

case class BadRequestResponse() extends Response {

  def status = BadRequest

  def body = "Bad request"

}