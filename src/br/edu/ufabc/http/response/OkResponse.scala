package br.edu.ufabc.http.response

import br.edu.ufabc.http.response.status.Ok

case class OkResponse(val body: String) extends Response {

  def status = Ok

}