package br.edu.ufabc.http.response

import br.edu.ufabc.http.response.status.NotFound
import br.edu.ufabc.http.route.Route

case class NotFoundResponse(private val route: Route) extends Response {

  def status = NotFound

  def body = "%s %s was not found".format(route.method, route.path)

}