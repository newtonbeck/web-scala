

package br.edu.ufabc.http

import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.ServerSocket
import scala.collection.mutable.Map
import scala.util.Failure
import scala.util.Success
import br.edu.ufabc.http.action.Action
import br.edu.ufabc.http.request.Request
import br.edu.ufabc.http.request.RequestFactory
import br.edu.ufabc.http.request.method.Get
import br.edu.ufabc.http.response.BadRequestResponse
import br.edu.ufabc.http.response.InternalServerErrorResponse
import br.edu.ufabc.http.response.NotFoundResponse
import br.edu.ufabc.http.response.OkResponse
import br.edu.ufabc.http.route.Route
import br.edu.ufabc.log.Logger
import akka.actor.ActorSystem
import akka.actor.Props
import br.edu.ufabc.worker.Worker
import br.edu.ufabc.worker.Message

case class HttpServer(val port: Int) {

  private val routes = Map[Route, Action]();

  def get(path: String, function: (Request) => (String)) =
    routes.put(Route(Get, path), Action(function))

  def proccess(httpMessage: String) = {
    RequestFactory.get(httpMessage) match {
      case Some(request) => {
        Logger.info("A requisição para %s foi entendida pelo servidor".format(request.route))
        routes.get(request.route) match {
          case Some(action) => {
            action.execute(request) match {
              case Success(body)      => { 
                Logger.info("A rota %s foi encontrada e sua action será executada".format(request.route))
                OkResponse(body).toHTTP
              }
              case Failure(exception) => { 
                Logger.info("A action da rota %s lançou uma exceção".format(request.route))
                InternalServerErrorResponse(exception).toHTTP
              }
            }
          }
          case None => { 
            Logger.info("A rota %s não foi encontrada no servidor".format(request.route))
            NotFoundResponse(request.route).toHTTP
          }
        }
      }
      case None => {
        Logger.info("A requisição não foi entendida pelo servidor")
        BadRequestResponse().toHTTP
      }
    }
  }

  def start(): Unit = {
    val system = ActorSystem("System")
    val worker = system.actorOf(Props[Worker])

    val socket = new ServerSocket(port)
    Logger.info("HttpServer started at %d port".format(port))

    while (true) {
      val client = socket.accept();
      worker ! Message(this, client)
    }
  }

}