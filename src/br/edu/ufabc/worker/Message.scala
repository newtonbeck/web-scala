package br.edu.ufabc.worker

import br.edu.ufabc.http.HttpServer
import java.net.Socket

case class Message(val httpServer: HttpServer, val socket: Socket)