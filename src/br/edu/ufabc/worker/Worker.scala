package br.edu.ufabc.worker

import akka.actor.Actor
import java.io.BufferedReader
import java.io.InputStreamReader

class Worker extends Actor {

  def receive: Actor.Receive = {
    case message: Message => {
      val input = new BufferedReader(new InputStreamReader(message.socket.getInputStream))
      val output = message.socket.getOutputStream

      // <uglyCode>
      var httpRequest = ""
      var linha = input.readLine()
      while (linha != null && !linha.isEmpty()) {
        httpRequest += "%s\r\n".format(linha)
        linha = input.readLine()
      }
      // </uglyCode>

      val httpResponse = message.httpServer.proccess(httpRequest)
      output.write(httpResponse.getBytes)
      message.socket.close()
    }
  }

}