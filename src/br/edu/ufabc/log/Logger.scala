package br.edu.ufabc.log

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

object Logger {

  def info(message: String) = {
    val currentThread = Thread.currentThread()
    val currentTime = LocalDateTime.now()
    val formatedDateTime = currentTime.format(DateTimeFormatter.ofPattern("HH:mm:ss:SSS"))
    println("[%s - %s] %s".format(currentThread.getName, formatedDateTime, message))
  }

}