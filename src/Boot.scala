

import br.edu.ufabc.http.HttpServer

object Boot extends App {

  val server = HttpServer(8888)

  server.get("/hello", (req) => { "<html>From hello</html>" })

  server.start()
  
}