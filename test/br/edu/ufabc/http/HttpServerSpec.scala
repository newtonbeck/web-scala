package br.edu.ufabc.http

import org.scalatest.FlatSpec
import org.scalatest.Matchers

class HttpServerSpec extends FlatSpec with Matchers {

  it should "return an Ok reponse" in {
    val httpRequest = "GET /hello HTTP/1.1\r\nHost: www.ufabc.edu.br\r\n\r\n"
    val server = HttpServer(80)
    server.get("/hello", (req) => { "Hello world" })

    val httpResponse = server.proccess(httpRequest);

    httpResponse should be("HTTP/1.1 200 OK\r\n\r\nHello world")
  }

  it should "return a Not Found reponse" in {
    val httpRequest = "GET /hello HTTP/1.1\r\nHost: www.ufabc.edu.br\r\n\r\n"
    val server = HttpServer(80)

    val httpResponse = server.proccess(httpRequest);

    httpResponse should be("HTTP/1.1 404 Not Found\r\n\r\nGet /hello was not found")
  }

  it should "return a Bad Request reponse" in {
    val httpRequest = "GET/helloHTTP/1.1\r\nHost: www.ufabc.edu.br\r\n"
    val server = HttpServer(80)

    val httpResponse = server.proccess(httpRequest);

    httpResponse should be("HTTP/1.1 400 Bad Request\r\n\r\nBad request")
  }

  it should "return an Internal Server Error reponse" in {
    val httpRequest = "GET /hello HTTP/1.1\r\nHost: www.ufabc.edu.br\r\n\r\n"
    val server = HttpServer(80)
    server.get("/hello", (req) => { throw new RuntimeException("This is an error") })
    val httpResponse = server.proccess(httpRequest);

    httpResponse should be("HTTP/1.1 500 Internal Server Error\r\n\r\nInternal Server Error caused by: This is an error")
  }

}