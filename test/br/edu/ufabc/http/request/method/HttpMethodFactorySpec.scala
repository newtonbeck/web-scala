package br.edu.ufabc.http.request.method

import org.scalatest.FlatSpec
import org.scalatest.Matchers

class HttpMethodFactorySpec extends FlatSpec with Matchers {

  it should "return Get HTTP method for GET String" in {
    val method = HttpMethodFactory get "GET"
    method should be(Some(Get))
  }

  it should "return None HTTP method for any non GET HTTP method String" in {
    val method = HttpMethodFactory get "FOO"
    method should be(None)
  }

}