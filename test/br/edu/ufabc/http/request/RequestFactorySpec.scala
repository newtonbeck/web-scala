package br.edu.ufabc.http.request

import org.scalatest.FlatSpec
import org.scalatest.Matchers

import br.edu.ufabc.http.request.method.Get
import br.edu.ufabc.http.route.Route

class RequestFactorySpec extends FlatSpec with Matchers {

  it should "return Some Request to a simple HTTP GET message" in {
    val simpleGetMessage = "GET /prograd/aluno HTTP/1.1\r\nHost: www.ufabc.edu.br\r\n\r\nq=matricula"

    val request = RequestFactory.get(simpleGetMessage)

    val route = Route(Get, "/prograd/aluno")
    val headers = Map[String, String]("Host" -> "www.ufabc.edu.br")
    request should be(Some(Request(route, headers)))
  }

  it should "return None Request to an HTTP message without route" in {
    val simpleGetMessage = "Host: www.ufabc.edu.br\r\n\r\nq=matricula"

    val request = RequestFactory.get(simpleGetMessage)

    request should be(None)
  }

  it should "return None Request to an empty HTTP message" in {
    val simpleGetMessage = ""

    val request = RequestFactory.get(simpleGetMessage)

    request should be(None)
  }

}