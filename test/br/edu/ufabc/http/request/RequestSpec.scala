package br.edu.ufabc.http.request

import org.scalatest.FlatSpec
import org.scalatest.Matchers

import br.edu.ufabc.http.request.method.Get
import br.edu.ufabc.http.route.Route

class RequestSpec extends FlatSpec with Matchers {

  it should "return None to an absent header" in {
    val headers = Map[String, String]()
    val request = new Request(Route(Get, "/foo"), headers)
    request.header("bar") should be(None)
  }

  it should "return Some to a present header" in {
    val headers = Map[String, String]("bar" -> "baz")
    val params = Map[String, String]()
    val request = new Request(Route(Get, "/foo"), headers)
    request.header("bar") should be(Some("baz"))
  }

}