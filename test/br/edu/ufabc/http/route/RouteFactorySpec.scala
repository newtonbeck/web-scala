package br.edu.ufabc.http.route

import org.scalatest.FlatSpec
import org.scalatest.Matchers
import br.edu.ufabc.http.request.method.Get

class RouteFactorySpec extends FlatSpec with Matchers {

  it should "return a Route for a valid HTTP GET message" in {
    val httpFirstLine = "GET /foo/bar HTTP/1.1";
    val route = RouteFactory.get(httpFirstLine)
    route should be(Some(Route(Get, "/foo/bar")))
  }

  it should "return None Route for an HTTP message without spaces" in {
    val httpFirstLine = "BAZ/foo/barHTTP/1.1";
    val route = RouteFactory.get(httpFirstLine)
    route should be(None)
  }

  it should "return None Route for an invalid HTTP method message" in {
    val httpFirstLine = "BAZ /foo/bar HTTP/1.1";
    val route = RouteFactory.get(httpFirstLine)
    route should be(None)
  }

}