package br.edu.ufabc.http.action

import scala.util.Failure
import scala.util.Success
import org.scalatest.FlatSpec
import org.scalatest.Matchers
import br.edu.ufabc.http.request.method.Get
import br.edu.ufabc.http.request.Request
import br.edu.ufabc.http.response.Response
import br.edu.ufabc.http.route.Route
import br.edu.ufabc.http.response.OkResponse

class ActionSpec extends FlatSpec with Matchers {

  it should "return a Success Response when function goes right" in {
    val request = Request(Route(Get, "/foo/bar"), Map[String, String]())
    val action = Action((req) => { "Hello World" })

    val response = action.execute(request)

    response should be(Success("Hello World"))
  }

  it should "return a Failure Response when function goes wrong" in {
    val request = Request(Route(Get, "/foo/bar"), Map[String, String]())
    val exception = new RuntimeException
    val action = Action((req) => { throw exception })

    val response = action.execute(request)

    response should be(Failure(exception))
  }

}